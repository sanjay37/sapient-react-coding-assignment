import React from 'react';
import './dashboard.css';
import { fetchStore } from './store/actions/actions';
import { connect } from 'react-redux';

const DashBoard = ({ spaceX }) => {
    console.log("spaceX", spaceX);
    return (
        <div className="container">
            <h1>SpaceX Launch Programs</h1>
            <div className="dashboard">
                <div className="filterViewContainer">
                    <div className="filter">
                        <h4>Filters</h4>
                        <div>
                            <p className="launchYear">Launch Year</p>
                            <div className="filterYear">
                                <div className="button">  
                                    <button type="button" className="">2014</button>
                                </div>
                                <div className="button">  
                                    <button type="button">2014</button>
                                </div>
                                <div className="button">  
                                    <button type="button">2014</button>
                                </div>
                                <div className="button">  
                                    <button type="button">2014</button>
                                </div>
                                <div className="button">  
                                    <button type="button">2014</button>
                                </div>
                                <div className="button">  
                                    <button type="button">2014</button>
                                </div>
                                <div className="button">  
                                    <button type="button">2014</button>
                                </div>
                                <div className="button">  
                                    <button type="button">2014</button>
                                </div>
                            </div>
                        </div>
                        <div>
                            <p className="launchYear">Successfull Launch</p>
                            <div className="filterYear">
                                <div className="button">  
                                    <button type="button">True</button>
                                </div>
                                <div className="button">  
                                    <button type="button">False</button>
                                </div>
                            </div>
                        </div>
                        <div>
                            <p className="launchYear">Successfull Landing</p>
                            <div className="filterYear">
                                <div className="button">  
                                    <button type="button">True</button>
                                </div>
                                <div className="button">  
                                    <button type="button">False</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="dataViewContainer">
                    { spaceX.map(( data, i ) => {
                        return (
                            <div className="detailBox" key={ i }>
                                <div className="image">
                                    <img 
                                        src={data.links.mission_patch} 
                                        className="mission-image" 
                                        alt="Mission Image"
                                    />
                                </div>
                                <h3>{data.mission_name} #{data.flight_number}</h3>
                                <div>
                                    <ul>
                                        <li>Mission List Id</li>
                                    </ul>
                                </div>
                                <p>Launch Year:{data.launch_year}</p>
                                <p>Successfull Launch:{data.launch_success}</p>
                                <p>Successfull Landing:{data.launch_landing}</p>
                            </div>
                        )
                    }) }
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = ( state ) => {
    return state
}

export default connect(
    mapStateToProps,
    fetchStore
)( DashBoard );
