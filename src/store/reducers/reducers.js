import { POPULATE_STORE } from "../actions/actions";

export default( state={}, action ) => {
    console.log("response", action);
    switch( action.type )
    {
        case POPULATE_STORE: {
            return action.payload.response
        }

        default: {
            return state;
        }
    }
}