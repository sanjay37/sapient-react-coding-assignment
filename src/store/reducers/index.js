import { combineReducers } from "redux";
import spaceX from "./reducers";

export default combineReducers({
    spaceX
});