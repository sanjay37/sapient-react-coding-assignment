import buildFetch from './build-fetch';

export const POPULATE_STORE = "POPULATE_STORE";
export const populateStore = ( dispatch, response ) => {
    dispatch({
        type: POPULATE_STORE,
        payload: {
            response
        }
    })
}

export const fetchStore = () => {
    return( dispatch, getStore ) => {
        return fetch(`https://api.spaceXdata.com/v3/launches?limit=100`, buildFetch('GET'))
            .then( response => response.json() )
            .then( response => {
                populateStore( dispatch, response );
            } )
    }
}