export default (
    method = 'GET',
    body
  ) => {
    const info = {
      method,
      headers: {
        'Content-Type': 'application/vnd.com.shoebuy.v1+json',
      },
      credentials: 'same-origin'
    }
    if ( body )
    {
      info.body = JSON.stringify( body )
    }
    return info;
  }
  